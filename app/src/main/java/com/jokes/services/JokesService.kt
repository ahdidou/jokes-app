package com.jokes.services
import com.jokes.entities.Joke
import retrofit2.Call
import retrofit2.http.GET

interface JokesService {
    /**
     * Fetches 10 jokes
     */
    @GET("ten")
    fun listJokes(): Call<List<Joke>>
}