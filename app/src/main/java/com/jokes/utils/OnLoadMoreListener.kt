package com.jokes.utils

interface OnLoadMoreListener {
    fun onLoadMore()
}