package com.jokes.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jokes.entities.Joke
import com.jokes.services.JokesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class JokeViewModel: ViewModel() {
    private val _baseURL = "https://official-joke-api.appspot.com/jokes/"

    private val _jokes = ArrayList<Joke>()
    val jokes : ArrayList<Joke>
        get() = _jokes

    // Observable list of jokes
    private var _jokesLiveData = MutableLiveData<List<Joke>>()
    val jokesLiveData : LiveData<List<Joke>>
        get() = _jokesLiveData

    // true if network request fails
    private var _didFail = MutableLiveData<Boolean>()
    val didFail : LiveData<Boolean>
        get() = _didFail

    /**
     * Launches a network request to fetch jokes
     */
    fun fetchJokes() {
        val retrofit = Retrofit.Builder()
            .baseUrl(_baseURL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        val service: JokesService = retrofit.create(JokesService::class.java)

        val call: Call<List<Joke>> = service.listJokes()

        call.enqueue(object : Callback<List<Joke>> {
            override fun onResponse(call: Call<List<Joke>>, response: Response<List<Joke>>) {
                // Reset didFail value to false to hide the reload button
                _didFail.value = false
                if (response.code() == 200) {
                    response.body()?.let { _jokes.addAll(it) }
                    _jokesLiveData.value = _jokes.distinctBy{ it.id }
                }
            }
            override fun onFailure(call: Call<List<Joke>>, t: Throwable) {
                _didFail.value = true
            }
        })
    }
}