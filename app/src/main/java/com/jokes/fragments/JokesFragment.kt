package com.jokes.fragments

import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jokes.R
import com.jokes.adapters.JokesAdapter
import com.jokes.databinding.FragmentJokesBinding
import com.jokes.utils.OnLoadMoreListener
import com.jokes.utils.RecyclerViewLoadMoreScroll
import com.jokes.viewmodels.JokeViewModel
import java.util.*

class JokesFragment : Fragment() {
    //Binding object
    private lateinit var binding: FragmentJokesBinding

    //View model responsible of holding data and business logic
    private val viewModel: JokeViewModel by viewModels()

    //Recycler view Adapter
    private lateinit var jokesAdapter: JokesAdapter

    //Scroll listener to help add infinite scroll functionality
    lateinit var scrollListener: RecyclerViewLoadMoreScroll

    //Recycler view layout manager, in our case it's vertical
    lateinit var layoutManager:RecyclerView.LayoutManager

    //TextToSpeech service
    lateinit var textToSpeech: TextToSpeech

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_jokes, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setting up two way data binding
        binding.jokesViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        // Initializing the TextToSpeech service
        initTextToSpeech()

        // Initializing RecyclerView Adapter by passing an instance of TTS service
        jokesAdapter = JokesAdapter(textToSpeech)
        jokesAdapter.setOnItemClickListener(object : JokesAdapter.ClickListener {
            override fun onItemClick(v: View, position: Int) {
                val action = JokesFragmentDirections.actionJokesFragmentToDetailsFragment()
                findNavController().navigate(action)
            }
        })

        // Observe jokes' live data change to reload the Recyclerview when new data is available
        viewModel.jokesLiveData.observe(viewLifecycleOwner,
            { newData ->
                scrollListener.setLoaded()
                jokesAdapter.removeLoadingView()
                jokesAdapter.dataset = newData.toMutableList()
                jokesAdapter.notifyDataSetChanged()
            })

        // Observe jokes live data change to reload the Recyclerview When new data is available
        viewModel.didFail.observe(viewLifecycleOwner,
            { newValue ->
                if (newValue) {
                    jokesAdapter.removeLoadingView()
                    Toast.makeText(activity, getString(R.string.error_message),
                        Toast.LENGTH_LONG).show()
                }
            })

        // Setting the Recyclerview Adapter
        binding.jokesRecyclerView.adapter = jokesAdapter

        // Set the Layout Manager of the RecyclerView
        setLayoutManager()

        // Set the scrollListener of the RecyclerView
        setScrollListener()

        // Launch the first request to fetch jokes
        if(viewModel.jokes.size == 0) {
            this.fetchData()
        }
    }

    /**
     * Init TextToSpeech Service
     */
    private fun initTextToSpeech() {
        this.textToSpeech = TextToSpeech(context) { status ->
            if (status != TextToSpeech.ERROR) {
                //if there is no error then set language
                textToSpeech.language = Locale.US
            }else {
                // Notify the user with a Toast
                Toast.makeText(activity, getString(R.string.text_to_speech_error_message),
                    Toast.LENGTH_LONG).show()
            }
        }
    }

    /**
     * Launches a request to fetch jokes and adds loader to the RecyclerView
     */
    private fun fetchData() {
        jokesAdapter.addLoadingView()
        viewModel.fetchJokes()
    }

    /**
     * Sets the layout manager of the RecyclerView
     */
    private fun setLayoutManager() {
        layoutManager = LinearLayoutManager(activity)
        binding.jokesRecyclerView.layoutManager = layoutManager
        binding.jokesRecyclerView.setHasFixedSize(false)
    }

    /**
     * Sets ScrollListener of the RecyclerView
     */
    private  fun setScrollListener() {
        layoutManager = LinearLayoutManager(activity)
        scrollListener = RecyclerViewLoadMoreScroll(layoutManager as LinearLayoutManager)
        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                fetchData()
            }
        })
        binding.jokesRecyclerView.addOnScrollListener(scrollListener)
    }
}