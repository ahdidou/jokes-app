package com.jokes.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jokes.databinding.FragmentDetailsBinding
import com.squareup.picasso.Picasso
import kotlin.random.Random

class DetailsFragment : Fragment() {
    private lateinit var binding: FragmentDetailsBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val randomInt = Random.nextInt(100)
        Picasso.get().
            load("https://picsum.photos/1200/800?random=$randomInt").into(binding.randomImage)
    }
}