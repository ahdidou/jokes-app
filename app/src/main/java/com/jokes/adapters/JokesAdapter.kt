package com.jokes.adapters

import android.content.Context
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jokes.R
import com.jokes.entities.Joke
import com.jokes.utils.Constant
import com.squareup.picasso.Picasso
import kotlin.random.Random

class JokesAdapter(private val textToSpeech: TextToSpeech)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>()  {
    // Initialise an empty dataset
    var dataset: MutableList<Joke?> = mutableListOf()

    lateinit var context: Context

    private var clickListener: ClickListener? = null

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener{
        val setupTV: TextView = view.findViewById(R.id.setup)
        val punchlineTV: TextView = view.findViewById(R.id.punchline)
        val randomIV: ImageView = view.findViewById(R.id.random_image)
        val micButton: ImageButton = view.findViewById(R.id.mic_button)
        val likesTV: TextView = view.findViewById(R.id.likes_tv)
        val commentsTV: TextView = view.findViewById(R.id.comments_tv)

        init {
            if (clickListener != null) {
                view.setOnClickListener(this)
            }
        }

        override fun onClick(v: View?) {
            if (v != null) {
                clickListener?.onItemClick(v,adapterPosition)
            }
        }
    }

    class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return if (viewType == Constant.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(context)
                .inflate(R.layout.joke_row_item, parent, false)
            ItemViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(context)
                    .inflate(R.layout.progress_loading, parent, false)
            LoadingViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            val item = dataset?.get(position)
            if (item != null) {

                (holder as ItemViewHolder).setupTV.text =  item.setup
                holder.punchlineTV.text =  item.punchline

                val comments = Random.nextInt(100)
                holder.commentsTV.text = "$comments"

                val likes = Random.nextInt(100)
                holder.likesTV.text = "$likes"

                Picasso.get().
                load("https://picsum.photos/1200/800?random=$position").into(holder.randomIV)

                holder.micButton.setOnClickListener {
                    textToSpeech.speak(item.setup + "" + item.punchline, TextToSpeech.QUEUE_FLUSH, null, null)
                }
            }
        }

    }
    override fun getItemCount() = dataset?.size

    override fun getItemViewType(position: Int): Int {
        return if (dataset[position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }


    // Adds a loader at the bottom of the RecyclerView
    fun addLoadingView() {
        //Add loading item
        Handler().post {
            dataset.add(null)
            notifyItemInserted(dataset.size - 1)
        }
    }

    // Remove a loader at the bottom of the RecyclerView
    fun removeLoadingView() {
        if (dataset.size != 0) {
            dataset.removeAt(dataset.size - 1)
            notifyItemRemoved(dataset.size)
        }
    }

    fun setOnItemClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    interface ClickListener {
        fun onItemClick(v: View,position: Int)
    }
}